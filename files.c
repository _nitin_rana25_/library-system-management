#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#define clrscr system("clear"); //To clear screen

//to get to the (x,y) coordinates
void gotoxy(int x,int y)     
{
    printf("%c[%d;%df",0x1B,y,x);
}


void add_books();
void search_books();
void main_scr();
void view_books();

int strcm(char *c, char *d);

char booknm[20];
char author[20];

struct books
{
	int id;
	char name[20];
	char Author[20];
	int quantity;
	float Price;
};


int main()
{
printf("\e[48;2;0;0;0m Background color: white\n");
	main_scr();
	//view_books(); 
}


void add_books()
{
	
	clrscr;
	 FILE *fptr;
	 struct books b;
	 
	 int i=0;

	   
  	 fptr = fopen("C:\\program.txt","a+");	

  	 if(fptr == NULL)
 	  {
    	  printf("Error!");   
      	exit(1);             
   	  }
	fflush(stdin);
	printf("Enter book ID: ");
	scanf("%d", &b.id);
	fflush(stdin);
	
   	printf("Enter book name: ");
  	scanf("%s", b.name);
  	fflush(stdin);
  	printf("Enter Author name: ");
  	
   	scanf("%s", b.Author);
   	fflush(stdin);
   	printf("Enter quatity: ");
   	scanf("%d", &b.quantity);
   	fflush(stdin);
   	printf("Enter price: ");
   	scanf("%f", &b.Price);
   	fflush(stdin);
	
   	fseek(fptr, 0, SEEK_END);
	fwrite(&b,sizeof(b),1,fptr);
	fclose(fptr);
	
   	
}

 void search_books()
{
   	clrscr;
   	
   	int d;
   	struct books b;
   	
   	FILE *fptr;
   	fptr = fopen("C:\\program.txt","r");
  	
   	int ID;
   	char name[20];
   	gotoxy(20,11);
   	
   	//scanf("%d", &ID);
	clrscr;
   	if ((fptr = fopen("C:\\program.txt","r")) == NULL){
   	    printf("Error! opening file");
   	    exit(1);
   	}
   	
   	//Options for search types
   	gotoxy(20,11);
   	printf("Chose an option: ");
   	scanf("%d", &d);
	
	switch(d) {
	case 1:
	fseek(fptr, 0, SEEK_SET);
	gotoxy(20,11);
	printf("Enter book's ID: ");
	scanf("%d", &ID);
   	while(fread(&b,sizeof(b),1,fptr)==1)
	if(b.id == ID)
	{
	gotoxy(20,7);
	printf("The Book is available");
	gotoxy(20,9);
	printf(" ID:%d",b.id);
	gotoxy(20,10);
	printf(" Name:%s",b.name);
	gotoxy(20,11);
	printf(" Author:%s ",b.Author);
	gotoxy(20,12);
	printf(" Qantity:%d ",b.quantity);
	gotoxy(20,13);
	printf(" Price:Rs.%.2f",b.Price);
	gotoxy(40,13);
	}
	break;
	case 2:
	fseek(fptr, 0, SEEK_SET);
	gotoxy(20,11);
	printf("Enter book's name: ");
	scanf("%s", booknm);
	clrscr;
   	while(fread(&b,sizeof(b),1,fptr)==1)
	if(strcm(booknm, b.name))
	{
	gotoxy(20,7);
	printf("The Book is available");
	gotoxy(20,9);
	printf(" ID:%d",b.id);
	gotoxy(20,10);
	printf(" Name:%s",b.name);
	gotoxy(20,11);
	printf(" Author:%s ",b.Author);
	gotoxy(20,12);
	printf(" Qantity:%d ",b.quantity);
	gotoxy(20,13);
	printf(" Price:Rs.%.2f",b.Price);
	gotoxy(40,13);
	}
	break;
	
	
	case 3:
	fseek(fptr, 0, SEEK_SET);
	gotoxy(20,11);
	printf("Enter book's Author name: ");
	scanf("%s", author);
	clrscr;
   	while(fread(&b,sizeof(b),1,fptr)==1)
	if(strcm(author, b.Author))
	{
	gotoxy(20,7);
	printf("The Book is available");
	gotoxy(20,9);
	printf(" ID:%d",b.id);
	gotoxy(20,10);
	printf(" Name:%s",b.name);
	gotoxy(20,11);
	printf(" Author:%s ",b.Author);
	gotoxy(20,12);
	printf(" Qantity:%d ",b.quantity);
	gotoxy(20,13);
	printf(" Price:Rs.%.2f",b.Price);
	gotoxy(40,13);
	}
	break;
	
	}
	

	
   	fclose(fptr);
   	
}

void main_scr()
{
	char c;

	clrscr;
	
	gotoxy(30,8);
	printf("1. Add Book");
	gotoxy(30, 9);
	printf("2. Delete Book");
	gotoxy(30, 10);
	printf("3. Search Book");
	gotoxy(30, 11);
	printf("4. Issue Book");
	gotoxy(30, 12);
	printf("5. View Books");
	gotoxy(30, 13);
	printf("6. Add Book");	
	gotoxy(0,0);
	c = getchar();
	switch(c) 
	{
	case '1':
		add_books();
		break;
	case '3':
		search_books();
		break;
	case '5':
		view_books();
		break;
	}
}

//Function for string's comparison
int strcm(char *c, char *d)
{
	int i;
	int res = 1;
	for (i = 0; c[i] != '\0' ; i++)
	{
	if(c[i] == d[i]){}
	else
	{ 
	res = 0;
	}
	return res;
	}
}

void view_books()
{
	FILE *fptr;
	struct books b;
	
	fptr = fopen("C:\\program.txt", "r");
	if ((fptr) == NULL)
		printf("Error! no data yet. ");
		
	fseek(fptr, 0, SEEK_SET);
	
	clrscr;
	gotoxy(1,1);
	printf("*****************************************Book List************************************");
	gotoxy(2,2);
	printf(" CATEGORY   ID         BOOK NAME           AUTHOR              QTY         PRICE\n");
	while(fread(&b, sizeof(b), 1, fptr))
	{
		printf("             %-11d%-20s%-20s%-12d%-6.2f\n", b.id,b.name,b.Author,b.quantity, b.Price);
		
	}
	fclose(fptr);
	gotoxy(40,10);
	fflush(stdin);
	char c[10];
	scanf("%s", c);
	
}



